//these are going directly into the scripts.js file now

module.exports = [
	{
		find: {
			id: 'home-main',
			type: 'slot'
		},
		replace: 'w-home-slot1-march1-week1'
	},
	{
		find: {
			id: 'home-bottom-combined',
			type: 'slot'
		},
		replace: 'w-home-slot2-march1-week1'
	},
	{
		find: {
			id: 'home-bottom-hero',
			type: 'slot'
		},
		replace: 'w-home-slot4-march1-week1'
	},
	{
		find: {
			id: 'home-bottom',
			type: 'slot'
		},
		replace: 'w-home-slot5-march1-week1'
	},
	{
		find: {
			id: 'home-bottom-social',
			type: 'slot'
		},
		replace: 'w-home-slot6-march1-week1'
	}
];