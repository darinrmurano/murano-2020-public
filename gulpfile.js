const gulp = require('gulp'),
	sass = require('gulp-sass'),
	path = require('path'),
	data = require('gulp-data'),
	merge = require('gulp-merge-json'),
	fs = require('fs'),
	camelCase = require('camelcase'),
	pug = require('gulp-pug'),
	browserify = require('browserify'),
	browserSync = require('browser-sync').create();

function message(done) {
	console.log("Gulp is running...");
	done();
}

function pug_data() {
	return gulp
		.src('src/data/**/*.json')
		.pipe(merge({
			fileName: 'data.json',
			edit: (json, file) => {
				var filename = path.basename(file.path),
					primaryKey = filename.replace(path.extname(filename), '');
				var data = {};
				data[camelCase(primaryKey)] = json;
				return data;
			}
		}))
		.pipe(gulp.dest('temp'))
}

function html() {
	return gulp.src([
			'src/html/**/*.pug',
			'src/html/**/*.html',
			'!src/html/**/_*.pug'
		])
		.pipe(data(function() {
			return JSON.parse(fs.readFileSync('temp/data.json'));
		}))
		.pipe(pug({
			locals: require('./locals.js'),
			pretty: true,
			basedir: process.cwd() + '/src/html'
		}))
		.pipe(gulp.dest('build/html'))
}


function script() {
	return gulp.src([
			'src/js/**/*.js',
			'src/js/**/_*.js'
		//], {read: false}) // no need of reading file because browserify does.
		])
		.pipe(gulp.dest('build/js'))
		.pipe(browserSync.stream({once: true}));
}

function style() {
	return gulp.src('src/scss/**/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('build/css'))
		.pipe(browserSync.stream());
}

function watch() {
	browserSync.init({
		proxy: {
			target: 'https://www.muranophotography.com'
		},
		serveStatic: ['build'],
		port: 3031,
		rewriteRules: [
			{
				match: /(\/murano2020.css)/g,
				replace: '/murano2020.css'
			}
		],
		snippetOptions: {
			rule: {
				match: /<\/body>/i,
				fn: function(snippet, match) {
					return `
						<script src="/js/local-proxy/scripts.js"></script>
						${snippet}
						${match}
						`;
				}
			}
		}

		//server: {
		//	baseDir: './build/'
		//}
	})
	gulp.watch('src/data/**/*.json', pug_data).on('change', browserSync.reload);
	gulp.watch('src/html/**/*.pug', html).on('change', browserSync.reload);
	gulp.watch('src/html/**/*.html', html).on('change', browserSync.reload);
	gulp.watch('src/js/**/*.js', script).on('change', browserSync.reload);
	gulp.watch('src/scss/**/*.scss', style).on('change', browserSync.reload);
}

exports.message = message;
exports.pug_data = pug_data;
exports.html = html;
exports.script = script;
exports.style = style;
exports.watch = watch;

/*gulp.task('default', gulp.series('message', 'style', 'watch'));*/
/*gulp.task('default', gulp.parallel('message', 'sass'));*/
gulp.task('default', gulp.series(message, watch));