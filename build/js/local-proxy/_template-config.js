//these are going directly into the scripts.js file now

module.exports = [
	{
		find: {
			id: 'home-main',
			type: 'slot'
		},
		replace: 'w-home-slot1-march1-week1'
	},
	{
		find: {
			id: 'home-bottom-combined',
			type: 'slot'
		},
		replace: 'w-home-slot2-march1-week1'
	},
	{
		find: {
			id: 'home-bottom-hero',
			type: 'slot'
		},
		replace: 'w-home-slot4-march1-week1'
	},
	{
		find: {
			id: 'home-bottom',
			type: 'slot'
		},
		replace: 'w-home-slot5-march1-week1'
	},
	{
		find: {
			id: 'home-bottom-social',
			type: 'slot'
		},
		replace: 'w-home-slot6-march1-week1'
	},
	// {
	// 	find: {
	// 		id: 'mens-home-1',
	// 		type: 'slot'
	// 	},
	// 	replace: 'mens/men-home-hero-primary-jun2-week4'
	// },
	// {
	// 	find: {
	// 		id: 'mens-home-2',
	// 		type: 'slot'
	// 	},
	// 	replace: 'mens/men-home-middle-tiles-jun2-week4'
	// },
	// {
	// 	find: {
	// 		id: 'mens-home-3',
	// 		type: 'slot'
	// 	},
	// 	replace: 'mens/men-home-product-carousel'
	// },
	// {
	// 	find: {
	// 		id: 'mens-denim-7',
	// 		type: 'slot'
	// 	},
	// 	replace: 'modules/loyalty-tile-home'
	// },
	// {
	// 	find: {
	// 		id: 'mens-denim-6',
	// 		type: 'slot'
	// 	},
	// 	replace: 'modules/blog-carousel'
	// },
	// {
	// 	find: {
	// 		id: '5afba0a8398e0d865aa5f24846',
	// 		type: 'content'
	// 	},
	// 	replace: 'denim-report/denim-report-mens'
	// },
	// {
	// 	find: {
	// 		id: '161c41286d51df0ce8d80494bb',
	// 		type: 'content'
	// 	},
	// 	replace: 'footer/afterpay'
	// },
	// {
	// 	find: {
	// 		id: '8ad8054190b43b9950cdf5932d',
	// 		type: 'content'
	// 	},
	// 	replace: 'looks-we-love/looks-we-love-collection-june1-2020'
	// },
	// {
	// 	find: {
	// 		id: '3a1715a6c5c41eba7018197a48',
	// 		type: 'content'
	// 	},
	// 	replace: 'looks-we-love/looks-we-love-june1-2020'
	// },
	// {
	// 	find: {
	// 		id: '039faf69d509648f9cd5eb1dcb',
	// 		type: 'content'
	// 	},
	// 	replace: 'looks-we-love/mens-style-story-june-2020'
	// },
	// {
	// 	find: {
	// 		id: '5333b182e5b497584ea276cb6b',
	// 		type: 'content'
	// 	},
	// 	replace: 'footer/gift-card-faq'
	// },
	// {
	// 	find: {
	// 		id: 'c67fd716da71e9ca63003e6d7f',
	// 		type: 'content'
	// 	},
	// 	replace: 'gift-guide/content/sweaters'
	// },
	// {
	// 	find: {
	// 		id: 'c75cd6eb46ebbd878acf0b0ae7',
	// 		type: 'content'
	// 	},
	// 	replace: 'footer/loyalty-landing-page'
	// },
	// {
	// 	find: {
	// 		id: 'cat-banner',
	// 		type: 'slot'
	// 	},
	// 	replace: 'tee-shop'
	// },
	// {
	// 	find: {
	// 		id: 'generic-category-1',
	// 		type: 'slot'
	// 	},
	// 	replace: 'denim-report/denim-digest-women-slot-one'
	// },
	// {
	// 	find: {
	// 		id: '216d6d486d84e24072cb2478fa',
	// 		type: 'content'
	// 	},
	// 	replace: 'pdp/mar2-perfect-shirt'
	// },
	// {
	// 	find: {
	// 		id: 'de094708048862be7296d6d7a8',
	// 		type: 'content'
	// 	},
	// 	replace: 'do-well-hub-earth-day/do-well-covid'
	// },
	// {
	// 	find: {
	// 		id: 'aff2ad18989252ed78df30b36d',
	// 		type: 'content'
	// 	},
	// 	replace: 'footer/instore-screen-03-31-2020'
	// },
	// {
	// 	find: {
	// 		id: '263f5bb58b4c1904da2b9633a7',
	// 		type: 'content'
	// 	},
	// 	replace: 'hometown-heroes/collective-may-2020'
	// },
	// {
	// 	find: {
	// 		id: '283c5f46ba2238408aabfe09f4',
	// 		type: 'content'
	// 	},
	// 	replace: 'covid-hub/covid-giving-back'
	// },
	// {
	// 	find: {
	// 		id: '4a34729da4d717ff1280fd90a9',
	// 		type: 'content'
	// 	},
	// 	replace: 'looks-we-love/swim-jun1-2020-B'
	// },
	// {
	// 	find: {
	// 		id: '6755ae1f0c56395ddad6e4a7d2',
	// 		type: 'content'
	// 	},
	// 	replace: 'looks-we-love/madewell-free-and-easy-jun-2020'
	// },
	// {
	// 	find: {
	// 		id: '8a2d4ec5157b4e6e55c822cf0f',
	// 		type: 'content'
	// 	},
	// 	replace: 'looks-we-love/madewell-mens-free-and-easy-jun-2020'
	// },
	// {
	// 	find: {
	// 		id: '31aa342945dc4e32392714c00f',
	// 		type: 'content'
	// 	},
	// 	replace: 'looks-we-love/mens-everywear-shorts'
	// },
	// {
	// 	find: {
	// 		id: '8795cf651dfed01bf93b9f9c1a',
	// 		type: 'content'
	// 	},
	// 	replace: 'footer/instore-screen-06-02-2020'
	// },
	// {
	// 	find: {
	// 		id: '872dc46f4979501934a92b4f84',
	// 		type: 'content'
	// 	},
	// 	replace: 'looks-we-love/ugc-getaway-jun-2-2020'
	// }
];