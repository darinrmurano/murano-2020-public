if (window.jQuery) {
  $(function() {
    
    var templates = [
      {
        find: {
          id: 'home-main',
          type: 'slot'
        },
        replace: 'home-page/w-home-slot1-march1-week1'
      },
      {
        find: {
          id: 'home-bottom-combined',
          type: 'slot'
        },
        replace: 'home-page/w-home-slot2-march1-week1'
      },
      {
        find: {
          id: 'home-bottom-hero',
          type: 'slot'
        },
        replace: 'home-page/w-home-slot4-march1-week1'
      },
      {
        find: {
          id: 'home-bottom',
          type: 'slot'
        },
        replace: 'home-page/w-home-slot5-march1-week1'
      },
      {
        find: {
          id: 'home-bottom-social',
          type: 'slot'
        },
        replace: 'home-page/w-home-slot6-march1-week1'
      },
  {
    find: {
      id: '3a1715a6c5c41eba7018197a48',
      type: 'content'
    },
    replace: 'looks-we-love/looks-we-love-march1-2020'
  }
    ];

    var filter = function(node) {
      if (
        node.nodeValue.includes('dwMarker="slot"') ||
        node.nodeValue.includes('dwMarker="content"')
      ) {
        return NodeFilter.FILTER_ACCEPT;
      } else {
        return NodeFilter.FILTER_SKIP;
      }
    };

    function getAllComments(rootElem) {
      var comments = [];
      var iterator = document.createNodeIterator(
        rootElem,
        NodeFilter.SHOW_COMMENT,
        filter,
        false
      );
      var curNode;
      while ((curNode = iterator.nextNode())) {
        comments.push(curNode);
      }
      return comments;
    }

    function filterTemplates(comments) {
      var matches = [];
      comments.forEach(comment => {
        templates.forEach(template => {
          if (
            comment.nodeValue.includes(`dwMarker="${template.find.type}"`) &&
            comment.nodeValue.includes(`dwContentID="${template.find.id}"`)
          ) {
            matches.push({
              node: comment,
              templateData: template
            });
          }
        });
      });
      return matches;
    }

    function replaceHtml(match, i, matches) {
      var { node, templateData } = match;
      var targetEl = $(node).parent(),
        html = `/html/${templateData.replace}.html`;
      $.ajax(html)
        .done(response => {
          if (response.trim().startsWith('<!doctype')) {
            ajaxFail(targetEl, html);
            return false;
          }
          var message = `injected ${html} from local site`;
          targetEl
            .html(response)
            .prepend(
              `<!-- matched marker "${templateData.find.type}" and id "${templateData.find.id}", ${message} --><!-- ${node.nodeValue} -->`
            );
          if (i === matches.length - 1) {
            fireDOMContentLoaded();
          }
        })
        .fail(() => {
          ajaxFail(targetEl, html);
        });
    }

    function ajaxFail(targetEl, html) {
      var message = `Can't find ${html}`;
      var text = `Double check this path in _template-config.js. You need to restart gulp if ${html} is a new pug or html file.`;
      targetEl.prepend(
        `<p style="color:red;margin-bottom:0;font-weight:bold">${message}</p><p style="margin-top:0;">${text}</p>`
      );
      console.log(message);
    }

    function fireDOMContentLoaded() {
      window.document.dispatchEvent(
        new Event('DOMContentLoaded', {
          bubbles: true,
          cancelable: true
        })
      );
    }

    function init() {
      var matches = filterTemplates(getAllComments(document.body));
      matches.forEach((match, i) => {
        replaceHtml(match, i, matches);
      });
    }

    init();
  }); // end IIFE
} // end check for jQuery
