module.exports = {
	s7ImagePath: function(options) {
		var {
			image,
			width,
			height,
			fit,
			quality,
			format,
			extraParams,
			noSharpen
		} = options,
		params = `?wid=${width || '500'}${
			height ? `&hei=${height}` : ''
		}&fmt=${format || 'jpeg'}&fit=${fit || 'wrap'}&qlt=${quality || '75'},1${
			noSharpen ? '' : '&resMode=bisharp&op_usm=0.5,1,5,0'
		}${extraParams || ''}`;

		// return `https://www.muranophotography.com/images/${image}${params}`;
		return `https://www.muranophotography.com/images/${image}`;
	},
	getSrcSet: function(options) {
		var { sizes, image, params } = options;
		return `${sizes.map(size => {
			var imageOptions = { image: image, width: size, ...params };
			return `${module.exports.s7ImagePath(imageOptions)} ${size}w`;
		})}`;
	},
	buildLink: function(options) {
		var { text, type, params } = options;

		var attrs;
		switch (type) {
			case 'product':
				attrs = {
					controller: 'Product',
					param: 'pid'
				};
				break;
			case 'category':
				attrs = {
					controller: 'Search',
					param: 'cgid'
				};
				break;
			case 'page':
				attrs = {
					controller: 'Page',
					param: 'cid'
				};
				break;
			case 'folder':
				attrs = {
					controller: 'Search',
					function: 'ShowContent',
					param: 'fdid'
				};
				break;
			case 'search':
				attrs = {
					controller: 'Search',
					param: 'q'
				};
				break;
			default:
				attrs = null;
				break;
		}

		var output = attrs
			? `$url('${attrs.controller}-${attrs.function || 'Show'}','${
					attrs.param
				}','${text}')$`
			: text;

		return `${output}${params ? `?${params}` : ''}`;
	},
	addCaret: function(options) {
		return options.text.replace(
			/(\S+)\s*$/,
			`<span style="white-space:nowrap">$1<span class="${(options.caretClass &&
			options.caretClass) ||
			'caret'}"></span></span>`
		);
	},
	pascalCase: function(text) {
		var pascalCase = require('js-pascalcase');
		return pascalCase(text);
	},
	placeholderSrc: function(width, height) {
		return `data:image/svg+xml,%3Csvg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 ${width} ${height}" %3E%3Crect width='100%25' height='100%25' fill='%23f8f8f8' /%3E%3C/svg%3E`;
	}
};