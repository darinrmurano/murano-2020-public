<script>
    (function() {
        function initSliders() {
            $(".js-do-well-hub-slider").slick({
                arrows: false,
                dots: true,
                centerMode: true,
                infinite: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                variableWidth: true,
                variableHeight: true,
                responsive: [
                    {
                        breakpoint: 9999,
                        settings: "unslick"
                    },
                    {
                        breakpoint: 767,
                        settings: {
                            centerMode: false
                        }
                    }
                ]
            });
        }
        //initSliders();
        document.addEventListener( "DOMContentLoaded", initSliders )
    })();
    document.getElementById('not-active--mobile').addEventListener('click', function (e) {
        if (window.screen.width <= 768) {
            e.preventDefault();
        }
    });
</script>